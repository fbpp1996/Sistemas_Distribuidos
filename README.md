
las tablas utilizadas para el proyecto son:


CREATE TABLE persona
(
    cedula integer NOT NULL,
    nombre character varying(1000),
    apellido character varying(1000),
    CONSTRAINT pk_cedula PRIMARY KEY (cedula)
)
WITH (
   OIDS=FALSE
 );


create table asignaturas(
idasignatura integer not null,
nombreasignatura character varying(1000),
constraint pk_asignatura primary key(idasignatura)
)
WITH (
   OIDS=FALSE
 );

create table relacion(
idrelacion integer not null,
idasignatura integer references asignaturas(idasignatura),
cedula integer references persona(cedula)
CONSTRAINT pk_relacion PRIMARY KEY (idrelacion)
)
WITH (
   OIDS=FALSE
 );