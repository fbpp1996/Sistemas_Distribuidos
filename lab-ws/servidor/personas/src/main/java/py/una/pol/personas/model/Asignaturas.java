package py.una.pol.personas.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
/*Esta clase representa a las asignaturas en el que uno o mas alumnos pueden estar inscriptos */

@SuppressWarnings("serial")
@XmlRootElement
public class Asignaturas  implements Serializable {

	
	Long idAsignatura;
	//identificador de asignatura y su nombre
	String nombreAsignatura;
	
	List <Persona> Alumnos; 
	//alumnos inscriptos en la asignatura
	
	
	//constructores
	public Asignaturas() {
		Alumnos = new ArrayList<Persona>();
	}
	
	public Asignaturas(Long idDeAsignatura, String nombreDeAsignatura) {
		this.idAsignatura = idDeAsignatura;
		this.nombreAsignatura = nombreDeAsignatura;
		
		Alumnos = new ArrayList<Persona>();
	}
	
	//getters y setters
	public Long getId() {
		return this.idAsignatura ;
	}
	
	public void setID(Long IdAsignatura) {
		this.idAsignatura = IdAsignatura;
	}
	
	public String getNombre() {
		return this.nombreAsignatura;
	}
	
	public void setNombre(String nombre) {
		this.nombreAsignatura = nombre;
	}
	
	public List<Persona> getAlumnos(){
		return this.Alumnos;
	}
	
	public void setAumnos(List<Persona> Alumnos) {
		this.Alumnos = Alumnos;
	}
}
